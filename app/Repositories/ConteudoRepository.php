<?php

namespace App\Repositories;

use App\Models\Conteudo;
use App\Repositories\BaseRepository;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ConteudoRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'playlist_id',
        'title',
        'url',
        'author'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Conteudo::class;
    }

    public function paginate(int $perPage, array $columns = ['*']): LengthAwarePaginator
    {
        $query = $this->allQuery();

        $query->join('playlists', 'conteudos.playlist_id', '=', 'playlists.id')
            ->select('conteudos.*', 'playlists.title as playlist_name');

        return $query->paginate($perPage, $columns);
    }

    public function delete(int $id)
    {
        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        $conteudo = $model->toArray();

        \Log::channel('sectoTech')->info('Conteudo deletado: ' . json_encode(
            [
                'user' => auth()->user()->name,
                'user_id' => auth()->user()->id,
                'conteudo' => $conteudo
            ]
        ));

        return $model->delete();
    }
}
