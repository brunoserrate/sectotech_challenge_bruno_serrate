<?php

namespace App\Repositories;

use App\Models\Conteudo;
use App\Models\Playlist;
use App\Repositories\BaseRepository;

class PlaylistRepository extends BaseRepository
{
    protected $fieldSearchable = [
        'title',
        'description',
        'author'
    ];

    public function getFieldsSearchable(): array
    {
        return $this->fieldSearchable;
    }

    public function model(): string
    {
        return Playlist::class;
    }

    public function delete(int $id)
    {
        $query = $this->model->newQuery();

        $model = $query->findOrFail($id);

        $playlist = $model->toArray();

        $playlist['conteudos'] = Conteudo::where('playlist_id', $id)->get();

        \Log::channel('sectoTech')->info('Playlist deletado: ' . json_encode(
            [
                'user' => auth()->user()->name,
                'user_id' => auth()->user()->id,
                'playlist' => $playlist
            ]
        ));

        return $model->delete();
    }
}
