<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 use Illuminate\Database\Eloquent\Factories\HasFactory;
class Conteudo extends Model
{
    use HasFactory;    public $table = 'conteudos';

    public $fillable = [
        'playlist_id',
        'title',
        'url',
        'author'
    ];

    protected $casts = [
        'title' => 'string',
        'url' => 'string',
        'author' => 'string'
    ];

    public static array $rules = [
        'playlist_id' => 'required',
        'title' => 'required|string|max:150',
        'url' => 'required|string|max:255|url|active_url',
        'author' => 'nullable|string|max:150',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];

    public function playlist(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(\App\Models\Playlist::class, 'playlist_id');
    }
}
