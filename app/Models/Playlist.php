<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    public $table = 'playlists';

    public $fillable = [
        'title',
        'description',
        'author'
    ];

    protected $casts = [
        'title' => 'string',
        'description' => 'string',
        'author' => 'string'
    ];

    public static array $rules = [
        'title' => 'required|string|max:100',
        'description' => 'nullable|string|max:200',
        'author' => 'required|string|max:150',
        'created_at' => 'nullable',
        'updated_at' => 'nullable'
    ];
}
