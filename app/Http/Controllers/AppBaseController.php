<?php

namespace App\Http\Controllers;

use InfyOm\Generator\Utils\ResponseUtil;

/**
 * @OA\Server(url="/api")
 * @OA\Info(
 *   title="InfyOm Laravel Generator APIs",
 *   version="1.0.0"
 * )
 * This class should be parent class for other API controllers
 * Class AppBaseController
 */
class AppBaseController extends Controller
{
    public function sendResponse($result, $message)
    {
        return response()->json(ResponseUtil::makeResponse($message, $result));
    }

    public function sendError($error, $code = 404)
    {
        return response()->json(ResponseUtil::makeError($error), $code);
    }

    public function sendSuccess($message)
    {
        return response()->json([
            'success' => true,
            'message' => $message
        ], 200);
    }

    public function sendErrorWithFlashMessage($error, $flashHtml, $code = 404)
    {
        return response()->json([
            'error' => $error,
            'flash' => $flashHtml
        ], $code);
    }

    public function sendResponseWithFlashMessage($result, $message, $flashHtml)
    {
        $result['flash'] = $flashHtml;
        return response()->json(ResponseUtil::makeResponse($message, $result));
    }

    public function mountHtmlList($errors)
    {
        $html = "<ul>";
        foreach ($errors as $error) {
            $html .= "<li>" . $error . "</li>";
        }
        $html .= "</ul>";

        return $html;
    }
}
