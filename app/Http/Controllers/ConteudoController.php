<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateConteudoRequest;
use App\Http\Requests\UpdateConteudoRequest;
use App\Http\Controllers\AppBaseController;
use App\Repositories\ConteudoRepository;
use Illuminate\Http\Request;
use App\Models\Conteudo;

use Flash;
use Session;
use View;

use App\Models\Playlist;

class ConteudoController extends AppBaseController
{
    /** @var ConteudoRepository $conteudoRepository*/
    private $conteudoRepository;

    public function __construct(ConteudoRepository $conteudoRepo)
    {
        $this->conteudoRepository = $conteudoRepo;
    }

    /**
     * Display a listing of the Conteudo.
     */
    public function index(Request $request)
    {
        $conteudos = $this->conteudoRepository->paginate(10);

        if ($request->ajax()) {
            return view('conteudos.table')
                ->with('conteudos', $conteudos)
                ->with('total_pages', round($conteudos->total() / 10))
                ->render();
        }

        return view('conteudos.index')
            ->with('conteudos', $conteudos)
            ->with('total_pages', round($conteudos->total() / 10));
    }

    /**
     * Show the form for creating a new Conteudo.
     */
    public function create()
    {
        $playlists = Playlist::pluck('title', 'id')->toArray();
        return view('conteudos.create', compact('playlists'));
    }

    /**
     * Store a newly created Conteudo in storage.
     */
    public function store(CreateConteudoRequest $request)
    {
        try {
            $input = $request->all();

            $validator = \Validator::make($input, Conteudo::$rules);

            if ($validator->fails()) {
                $errors = $validator->errors()->all();

                Session::flash('error', "Given data was invalid: <br/> " . $this->mountHtmlList($errors));
                $validateFlash = View::make('partials/flash-messages')->render();

                return $this->sendErrorWithFlashMessage('Error saving conteudo', $validateFlash, 422);
            }

            Session::flash('success', 'Conteudo saved successfully.');
            $successflash = View::make('partials/flash-messages')->render();

            $conteudo = $this->conteudoRepository->create($input);

            return $this->sendResponseWithFlashMessage($conteudo, 'Conteudo saved successfully.', $successflash);
        }
        catch (\Throwable $th) {
            Session::flash('error', 'Error: ' . $th->getMessage());
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Error saving conteudo', $errorflash, 402);
        }
    }

    /**
     * Display the specified Conteudo.
     */
    public function show($id)
    {
        $conteudo = $this->conteudoRepository->find($id);

        if (empty($conteudo)) {
            Flash::error('Conteudo not found');

            return redirect(route('conteudos.index'));
        }

        $conteudo->playlist_name = '';

        $playlists = Playlist::where('id', $conteudo->playlist_id)->pluck('title', 'id')->toArray();

        if(!empty($playlists)) {
            $conteudo->playlist_name = $playlists[$conteudo->playlist_id];
        }

        return view('conteudos.show')->with('conteudo', $conteudo);
    }

    /**
     * Show the form for editing the specified Conteudo.
     */
    public function edit($id)
    {
        $conteudo = $this->conteudoRepository->find($id);

        if (empty($conteudo)) {
            Flash::error('Conteudo not found');

            return redirect(route('conteudos.index'));
        }

        $playlists = Playlist::pluck('title', 'id')->toArray();

        return view('conteudos.edit')->with('conteudo', $conteudo)->with('playlists', $playlists);
    }

    /**
     * Update the specified Conteudo in storage.
     */
    public function update($id, UpdateConteudoRequest $request)
    {
        $conteudo = $this->conteudoRepository->find($id);

        if (empty($conteudo)) {
            Session::flash('error', 'Conteudo not found');
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Error saving conteudo', $errorflash, 402);
        }

        try {
            $input = $request->all();

            $validator = \Validator::make($input, Conteudo::$rules);

            if ($validator->fails()) {
                $errors = $validator->errors()->all();

                Session::flash('error', "Given data was invalid: <br/> " . $this->mountHtmlList($errors));
                $validateFlash = View::make('partials/flash-messages')->render();

                return $this->sendErrorWithFlashMessage('Error saving conteudo', $validateFlash, 422);
            }

            Session::flash('success', 'Conteudo updated successfully.');
            $successflash = View::make('partials/flash-messages')->render();

            $conteudo = $this->conteudoRepository->update($input, $id);

            return $this->sendResponseWithFlashMessage($conteudo, 'Conteudo updated successfully.', $successflash);
        }
        catch (\Throwable $th) {
            Session::flash('error', 'Error: ' . $th->getMessage());
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Error saving conteudo', $errorflash, 402);
        }
    }

    /**
     * Remove the specified Conteudo from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $conteudo = $this->conteudoRepository->find($id);

        if (empty($conteudo)) {
            Session::flash('error', 'Conteudo not found');
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Error deleting conteudo', $errorflash, 404);
        }

        $this->conteudoRepository->delete($id);

        Session::flash('success', 'Conteudo deleted successfully.');
        $successflash = View::make('partials/flash-messages')->render();

        return $this->sendResponseWithFlashMessage([
            'id' => $id
        ], 'Conteudo deleted successfully.', $successflash);
    }
}
