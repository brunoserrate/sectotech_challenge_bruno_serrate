<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePlaylistRequest;
use App\Http\Requests\UpdatePlaylistRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Playlist;
use App\Repositories\PlaylistRepository;
use Illuminate\Http\Request;
use Flash;
use Session;
use View;

use function Termwind\render;

class PlaylistController extends AppBaseController
{

    /** @var PlaylistRepository $playlistRepository*/
    private $playlistRepository;

    public function __construct(PlaylistRepository $playlistRepo)
    {
        $this->playlistRepository = $playlistRepo;
    }

    /**
     * Display a listing of the Playlist.
     */
    public function index(Request $request)
    {
        $playlists = $this->playlistRepository->paginate(10);

        if($request->ajax()){
            return view('playlists.table')
                ->with('playlists', $playlists)
                ->with('total_pages', round($playlists->total() / 10))
                ->render();
        }

        return view('playlists.index')
            ->with('playlists', $playlists)
            ->with('total_pages', round($playlists->total() / 10));
    }

    /**
     * Show the form for creating a new Playlist.
     */
    public function create()
    {
        return view('playlists.create');
    }

    /**
     * Store a newly created Playlist in storage.
     */
    public function store(CreatePlaylistRequest $request)
    {
        try {
            $input = $request->all();

            $validator = \Validator::make($input, Playlist::$rules);

            if ($validator->fails()) {
                $errors = $validator->errors()->all();

                Session::flash('error', "Given data was invalid: <br/> " . $this->mountHtmlList($errors));
                $validateFlash = View::make('partials/flash-messages')->render();

                return $this->sendErrorWithFlashMessage('Error saving playlist', $validateFlash, 422);
            }

            Session::flash('success', 'Playlist saved successfully.');
            $successflash = View::make('partials/flash-messages')->render();

            $playlist = $this->playlistRepository->create($input);

            return $this->sendResponseWithFlashMessage($playlist, 'Playlist saved successfully.', $successflash);
        } catch (\Throwable $th) {
            Session::flash('error', 'Error: ' . $th->getMessage());
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Error saving playlist', $errorflash, 402);
        }
    }

    /**
     * Display the specified Playlist.
     */
    public function show($id)
    {
        $playlist = $this->playlistRepository->find($id);

        if (empty($playlist)) {
            Flash::error('Playlist not found');

            return redirect(route('playlists.index'));
        }

        $conteudo = \App\Models\Conteudo::where('playlist_id', $id)->get();

        return view('playlists.show')->with('playlist', $playlist)->with('conteudos', $conteudo);
    }

    /**
     * Show the form for editing the specified Playlist.
     */
    public function edit($id)
    {
        $playlist = $this->playlistRepository->find($id);

        if (empty($playlist)) {
            Flash::error('Playlist not found');

            return redirect(route('playlists.index'));
        }

        return view('playlists.edit')->with('playlist', $playlist);
    }

    /**
     * Update the specified Playlist in storage.
     */
    public function update($id, UpdatePlaylistRequest $request)
    {
        $playlist = $this->playlistRepository->find($id);

        if (empty($playlist)) {
            Session::flash('error', 'Playlist not found');
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Playlist not found', $errorflash, 404);
        }

        try {

            $input = $request->all();

            $validator = \Validator::make($input, Playlist::$rules);

            if ($validator->fails()) {
                $errors = $validator->errors()->all();

                Session::flash('error', "Given data was invalid: <br/> " . $this->mountHtmlList($errors));
                $validateFlash = View::make('partials/flash-messages')->render();

                return $this->sendErrorWithFlashMessage('Error updating playlist', $validateFlash, 422);
            }

            $playlist = $this->playlistRepository->update($input, $id);

            Session::flash('success', 'Playlist updated successfully.');
            $successflash = View::make('partials/flash-messages')->render();

            return $this->sendResponseWithFlashMessage($playlist, 'Playlist updated successfully.', $successflash);
        } catch (\Throwable $th) {

            Session::flash('error', 'Error: ' . $th->getMessage());
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Error updating playlist', $errorflash, 402);
        }
    }

    /**
     * Remove the specified Playlist from storage.
     *
     * @throws \Exception
     */
    public function destroy($id)
    {
        $playlist = $this->playlistRepository->find($id);

        if (empty($playlist)) {
            Session::flash('error', 'Playlist not found');
            $errorflash = View::make('partials/flash-messages')->render();

            return $this->sendErrorWithFlashMessage('Playlist not found', $errorflash, 404);
        }


        $this->playlistRepository->delete($id);

        Session::flash('success', 'Playlist deleted successfully.');
        $successflash = View::make('partials/flash-messages')->render();

        return $this->sendResponseWithFlashMessage([
            'id' => $id
        ], 'Playlist deleted successfully.', $successflash);
    }
}
