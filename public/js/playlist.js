jQuery(() => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
    });

    // Index
    $('#playlist-index-table').on('click', '.pagination a', function (e) {
        e.preventDefault();

        var page = $(this).attr('href').split('page=')[1];

        $('#hidden_page').val(page);

        $.ajax({
            url: "?page=" + page,
            success: function (data) {
                $('#playlist-index-table').html('');
                $('#playlist-index-table').html(data);
            }
        })
    });

    // Save / Edit
    $('#btn-save-playlist').on('click', function () {
        let param = {
            title: jQuery('#title').val(),
            description: jQuery('#description').val(),
            author: jQuery('#author').val(),
        }


        let id = $('#id').val();

        if (id) {
            param.id = id;

            $.ajax({
                url: '/playlists/' + id,
                type: 'PUT',
                data: param,
                success: function (response) {
                    if (response.success) {
                        $('div.flash-message').html(response.data.flash);

                        setTimeout(() => {
                            window.location.href = '/playlists';
                        }, 1500);
                    }
                },
                error: function (response) {
                    console.log(response);
                    $('div.flash-message').html(response.responseJSON.flash);
                }
            });

            return;
        }

        $.ajax({
            url: '/playlists',
            type: 'POST',
            data: param,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('div.flash-message').html(response.data.flash);

                    setTimeout(() => {
                        window.location.href = '/playlists';
                    }, 1500);
                }

            },
            error: function (response) {
                console.log(response);
                $('div.flash-message').html(response.responseJSON.flash)
            }
        });
    });

    // Delete
    $('.btn-delete-playlist').on('click', function (e) {
        e.preventDefault();
        let id = $(this).closest('tr').attr('data-id');

        if (confirm('Are you sure you want to delete this playlist?')) {
            $.ajax({
                url: '/playlists/' + id,
                type: 'DELETE',
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        $('div.flash-message').html(response.data.flash);

                        setTimeout(() => {
                            window.location.href = '/playlists';
                        }, 1500);
                    }
                },
                error: function (response) {
                    console.log(response);
                    $('div.flash-message').html(response.responseJSON.flash);
                }
            });
        }
    });

});