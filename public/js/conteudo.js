jQuery(() => {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
        },
    });

    // Index
    $('#conteudo-index-table').on('click', '.pagination a', function (e) {
        e.preventDefault();

        var page = $(this).attr('href').split('page=')[1];

        $('#hidden_page').val(page);

        $.ajax({
            url: "?page=" + page,
            success: function (data) {
                $('#conteudo-index-table').html('');
                $('#conteudo-index-table').html(data);
            }
        })
    });

    // Save / Edit
    $('#btn-save-conteudos').on('click', function () {
        let param = {
            playlist_id: $('#playlist_id').val(),
            title: $('#title').val(),
            url: $('#url').val(),
            author: $('#author').val(),
        }


        let id = $('#id').val();

        if (id) {
            param.id = id;

            $.ajax({
                url: '/conteudos/' + id,
                type: 'PUT',
                data: param,
                success: function (response) {
                    if (response.success) {
                        $('div.flash-message').html(response.data.flash);

                        setTimeout(() => {
                            window.location.href = '/conteudos';
                        }, 1500);
                    }
                },
                error: function (response) {
                    console.log(response);
                    $('div.flash-message').html(response.responseJSON.flash);
                }
            });

            return;
        }

        $.ajax({
            url: '/conteudos',
            type: 'POST',
            data: param,
            dataType: 'json',
            success: function (response) {
                if (response.success) {
                    $('div.flash-message').html(response.data.flash);

                    setTimeout(() => {
                        window.location.href = '/conteudos';
                    }, 1500);
                }

            },
            error: function (response) {
                console.log(response);
                $('div.flash-message').html(response.responseJSON.flash)
            }
        });
    });

    // Delete
    $('.btn-delete-conteudo').on('click', function (e) {
        e.preventDefault();
        let id = $(this).closest('tr').attr('data-id');

        if (confirm('Are you sure you want to delete this playlist?')) {
            $.ajax({
                dataType: 'json',
                url: '/conteudos/' + id,
                type: 'DELETE',
                success: function (response) {
                    if (response.success) {
                        $('div.flash-message').html(response.data.flash);

                        setTimeout(() => {
                            window.location.href = '/conteudos';
                        }, 1500);
                    }
                },
                error: function (response) {
                    console.log(response);
                    $('div.flash-message').html(response.responseJSON.flash);
                }
            });
        }
    });

});