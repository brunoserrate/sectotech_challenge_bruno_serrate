@extends('layouts.app')

@section('content')
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <h1>
                        Edit Conteudo
                    </h1>
                </div>
            </div>
        </div>
    </section>

    <div class="content px-3">

        @include('adminlte-templates::common.errors')

        <div class="flash-message"></div>

        <div class="card">

            {!! Form::model($conteudo) !!}

            <div class="card-body">
                <div class="row">
                    @include('conteudos.fields')
                </div>
            </div>

            <div class="card-footer">
                <a id="btn-save-conteudos" class="btn btn-primary"> Save </a>
                <a href="{{ route('conteudos.index') }}" class="btn btn-default"> Cancel </a>
            </div>

            {!! Form::close() !!}

        </div>
    </div>
@endsection
