<!-- Playlist Id Field -->
<div class="col-sm-12">
    {!! Form::label('playlist_name', 'Playlist:') !!}
    <p>{{ $conteudo->playlist_name }}</p>
</div>

<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $conteudo->title }}</p>
</div>

<!-- Url Field -->
<div class="col-sm-12">
    {!! Form::label('url', 'Url:') !!}
    <p>{{ $conteudo->url }}</p>
</div>

<!-- Author Field -->
<div class="col-sm-12">
    {!! Form::label('author', 'Author:') !!}
    <p>{{ $conteudo->author }}</p>
</div>

