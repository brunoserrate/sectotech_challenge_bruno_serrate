<div class="card-body p-0">
    <div class="table-responsive">
        <table class="table" id="conteudos-table">
            <thead>
                <tr>
                    <th>Playlist</th>
                    <th>Title</th>
                    <th>Url</th>
                    <th>Author</th>
                    <th colspan="3">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($conteudos as $conteudo)
                <tr data-id="{{ $conteudo->id }}">
                    <td>{{ $conteudo->playlist_name }}</td>
                    <td>{{ $conteudo->title }}</td>
                    <td>{{ $conteudo->url }}</td>
                    <td>{{ $conteudo->author }}</td>
                    <td style="width: 120px">
                        <div class='btn-group'>
                            <a href="{{ route('conteudos.show', [$conteudo->id]) }}" class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a href="{{ route('conteudos.edit', [$conteudo->id]) }}" class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            <a class="btn btn-danger btn-xs btn-delete-conteudo"><i class="far fa-trash-alt"></i></a>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            <div class="row">
                {!! $conteudos->links('custom.pagination') !!}
            </div>
        </div>
    </div>
</div>