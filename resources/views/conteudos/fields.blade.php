@if(isset($conteudo))
    <div class="form-group col-sm-2">
        {!! Form::label('id', 'ID:') !!}
        {!! Form::text('id', null, ['class' => 'form-control', 'readonly']) !!}
    </div>
    <div class="form-group col-sm-10"></div>
@endif

<!-- Playlist Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('playlist_id', 'Playlist:') !!}
    {!! Form::select('playlist_id', $playlists, null, ['class' => 'form-control', 'required']) !!}
</div>

<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'required', 'maxlength' => 150, 'maxlength' => 150]) !!}
</div>

<!-- Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('url', 'Url:') !!}
    {!! Form::text('url', null, ['class' => 'form-control', 'required', 'maxlength' => 255, 'maxlength' => 255, 'placeholder' => 'https://www.google.com.br']) !!}
    <small class="form-text text-muted">The URL must be valid and active, for example: https://www.google.com.br</small>
</div>

<!-- Author Field -->
<div class="form-group col-sm-6">
    {!! Form::label('author', 'Author:') !!}
    {!! Form::text('author', null, ['class' => 'form-control', 'maxlength' => 150, 'maxlength' => 150]) !!}
</div>