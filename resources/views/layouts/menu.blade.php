<li class="nav-item">
    <a href="{{ route('playlists.index') }}" class="nav-link {{ Request::is('playlists*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-list-alt"></i>
        <p>Playlists</p>
    </a>
</li>

<li class="nav-item">
    <a href="{{ route('conteudos.index') }}" class="nav-link {{ Request::is('conteudos*') ? 'active' : '' }}">
        <i class="nav-icon fas fa-music"></i>
        <p>Conteudos</p>
    </a>
</li>
