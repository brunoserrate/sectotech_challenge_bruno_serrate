<!-- Title Field -->
<div class="col-sm-12">
    {!! Form::label('title', 'Title:') !!}
    <p>{{ $playlist->title }}</p>
</div>

<!-- Description Field -->
<div class="col-sm-12">
    {!! Form::label('description', 'Description:') !!}
    <p>{{ $playlist->description }}</p>
</div>

<!-- Author Field -->
<div class="col-sm-12">
    {!! Form::label('author', 'Author:') !!}
    <p>{{ $playlist->author }}</p>
</div>

<!-- Conteudos playlist -->
<div class="col-sm-12">
    <h4 style="padding-bottom: 10px;">Conteudos:</h4>
    <div class="table-responsive">
        <table class="table" id="conteudos-table">
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Url</th>
                    <th>Author</th>
                </tr>
            </thead>
            <tbody>
                @foreach($conteudos as $conteudo)
                <tr>
                    <td>{{ $conteudo->title }}</td>
                    <td>{{ $conteudo->url }}</td>
                    <td>{{ $conteudo->author }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>