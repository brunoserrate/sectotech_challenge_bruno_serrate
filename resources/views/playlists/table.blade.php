<div id="playlist-index-table" class="card-body p-0">

    <div class="flash-message"></div>

    <div class="table-responsive">
        <table class="table" id="playlists-table">
            <thead>
            <tr>
                <th>Title</th>
                <th>Description</th>
                <th>Author</th>
                <th colspan="3">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($playlists as $playlist)
                <tr data-id="{{ $playlist->id }}">
                    <td>{{ $playlist->title }}</td>
                    <td>{{ $playlist->description }}</td>
                    <td>{{ $playlist->author }}</td>
                    <td  style="width: 120px">
                        <div class='btn-group'>
                            <a href="{{ route('playlists.show', [$playlist->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-eye"></i>
                            </a>
                            <a id="btn-edit-playlist" href="{{ route('playlists.edit', [$playlist->id]) }}"
                               class='btn btn-default btn-xs'>
                                <i class="far fa-edit"></i>
                            </a>
                            <a class="btn btn-danger btn-xs btn-delete-playlist"><i class="far fa-trash-alt"></i></a>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="card-footer clearfix">
        <div class="float-right">
            <div class="row">
                {!! $playlists->links('custom.pagination') !!}
            </div>
        </div>
    </div>
</div>
