<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Playlist>
 */
class PlaylistFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->text($this->faker->numberBetween(50, 150)),
            'description' => $this->faker->text($this->faker->numberBetween(50, 255)),
            'author' => $this->faker->name(),
            'created_at' => $this->faker->dateTime('Y-m-d H:i:s'),
            'updated_at' => $this->faker->dateTime('Y-m-d H:i:s'),
        ];
    }
}
